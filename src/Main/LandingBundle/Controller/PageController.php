<?php

namespace Main\LandingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('MainLandingBundle:Page:index.html.twig');
    }
    public function loginAction()
    {
        return $this->render('MainLandingBundle:Page:login.html.twig');
    }
    public function registerAction()
    {
        return $this->render('MainLandingBundle:Page:register.html.twig');
    }
    public function rankAction()
    {
        return $this->render('MainLandingBundle:Page:rank.html.twig');
    }
    public function statuteAction()
    {
        return $this->render('MainLandingBundle:Page:statute.html.twig');
    }
    public function contactAction()
    {
        return $this->render('MainLandingBundle:Page:contact.html.twig');
    }
}
