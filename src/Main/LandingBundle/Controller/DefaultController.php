<?php

namespace Main\LandingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MainLandingBundle:Default:index.html.twig', array('name' => 'test'));
    }
}
