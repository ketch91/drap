Symfony - wiki
========================

#After installation/clone a repository you should set cache/ and logs/ folders permissions. Execute:

sudo rm -rf app/cache/*
sudo rm -rf app/logs/*

$ sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
$ sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs

Important! Do not execute:
sudo chmod 777 app/cache app/logs

Now you can install composer.phar file in project folder,
curl -s http://getcomposer.org/installer | php

If any problems, try
curl -s http://getcomposer.org/installer | sudo php

Now you can update our project
php composer.phar update
